<?php 
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $zaporka = $_SESSION['zaporka'];
            if(strlen($zaporka) > 0) {
                $pwd = 1;
            } else {
                $pwd = 0;
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET zaporka='" . $pwd . "' WHERE sID='" . $studentID . "'";
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Zaporka</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Zaporka za račun elektroničke pošte
                    </h4>
                    <br/><br/>
                    <p class="contentbox">
                        Radi analize i procjene kvalitete zaporke molimo Vas napišite zaporku (lozinku) koju koristite za prijavu na vaš e-mail:
                        <br/><br/>
                        <input type="password" name="zaporka">
                    </p>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
            });
        </script>
    </body>
</html>