<?php
    $httpreferer = test_input($_SERVER['HTTP_REFERER']);
    if(!empty($httpreferer)){
        $parts = explode('/', $httpreferer);
        $script_referer = array_pop($parts);
        if($script_referer != prev($_SESSION['order'])) {
            header('Location: index.php');
        }
    } else {
        header('Location: index.php');
    }
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>