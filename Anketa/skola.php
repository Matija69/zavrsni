<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $skola1 = $_POST['skola1'];
            $skola2 = $_POST['skola2'];
            $sql = "UPDATE {$_SESSION['table_name']} SET p37='" . $skola1 . "', p38='" . $skola2 . "' WHERE sID='" . $studentID . "'";
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Ponašanje</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Odgovori na sljedeća pitanja:
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th class="textcentered"><h4 class="boldtext">Molim te da znakom X označiš 
                                koliko često se događa sljedeće:</h4>
                            </th>
                            <th>UVIJEK <br>(skoro svaki <br>dan)</th>
                            <th>ČESTO <br>(nekoliko puta <br>mjesečno)</th>
                            <th>PONEKAD <br>(jednom <br>mjesečno)</th>
                            <th>RIJETKO <br>(nekoliko puta <br>godišnje)</th>
                            <th>NIKAD</th>
                        </tr>
                        <tr>
                            <td>
                                1. Vrijeđaš drugu djecu putem Interneta (koristiš 
                                uvredljive nadimke, lažno se predstavljaš, šalješ
                                prijeteće ili uvredljive poruke putem facebooka,
                                chata, vibera ili sms-a i sl.).
                            </td>
                            <td class="textcentered"><input type="radio" name="skola1" value="1" data-toggle="tooltip" title="UVIJEK (skoro svaki dan)"></td>
                            <td class="textcentered"><input type="radio" name="skola1" value="2" data-toggle="tooltip" title="ČESTO (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="skola1" value="3" data-toggle="tooltip" title="PONEKAD (jednom mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="skola1" value="4" data-toggle="tooltip" title="RIJETKO (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="skola1" value="5" data-toggle="tooltip" title="NIKAD"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Tebe druga djeca vrijeđaju putem Interneta
                                (šalju ti prijeteće ili uvredljive poruke putem
                                facebooka, chata, vibera ili sms-a, dodjeljuju ti
                                uvredljive nadimke, predstavljaju se u tvoje ime
                                i sl.).
                            </td>
                            <td class="textcentered"><input type="radio" name="skola2" value="1" data-toggle="tooltip" title="UVIJEK (skoro svaki dan)"></td>
                            <td class="textcentered"><input type="radio" name="skola2" value="2" data-toggle="tooltip" title="ČESTO (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="skola2" value="3" data-toggle="tooltip" title="PONEKAD (jednom mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="skola2" value="4" data-toggle="tooltip" title="RIJETKO (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="skola2" value="5" data-toggle="tooltip" title="NIKAD"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault(); 
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>