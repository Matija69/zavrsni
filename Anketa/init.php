<?php

    session_start();

    $GLOBALS['config'] = array(
        $scripts_pupils     = array(
            "ucenici.php", 
            "ucestalost.php", 
            "stupanj_sigurnosti.php",
            "stupanj_uvjerenja.php",
            "stupanj_vaznosti.php",
            "sigurnost_osobnih_podataka.php",
            "zaporka.php",
            "skola.php",
            "ponasanja.php",
            "uek.php",
            "ioskala.php",
            "sseu1.php",
            "opci_podaci.php",
            "zahvala.php"
        ),
        $scripts_students   = array(
            "studenti.php",
            "ucestalost.php",
            "stupanj_sigurnosti.php",
            "stupanj_uvjerenja.php",
            "stupanj_vaznosti.php",
            "sigurnost_osobnih_podataka.php",
            "zaporka.php",
            "ponasanja_studenti.php",
            "zadovoljstvo_studenti.php",
            "stres_studenti.php",
            "zahvala.php"
        ),
        $scripts_employees  = array(
            "zaposlenici.php",
            "ucestalost.php",
            "stupanj_sigurnosti.php",
            "stupanj_uvjerenja.php",
            "stupanj_vaznosti.php",
            "sigurnost_osobnih_podataka.php",
            "zaporka.php",
            "posao.php",
            "zadovoljstvo_djelatnici.php",
            "stres_djelatnici.php",
            "zahvala.php"
        ),
        $schools            = array("ucenici1", "ucenici2"),
        $faculties          = array("fax1", "fax2"),
        $firms              = array("firm1", "firm2")
    );

?>