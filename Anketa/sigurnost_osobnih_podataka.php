<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $sigurnost_podataka1 = $_POST['sigurnost_podataka1'];
            $sigurnost_podataka2 = $_POST['sigurnost_podataka2'];
            $sigurnost_podataka3 = $_POST['sigurnost_podataka3'];
            $sql = "UPDATE {$_SESSION['table_name']} SET p34='" . $sigurnost_podataka1 . "', p35='" . $sigurnost_podataka2 . "', p36='" . $sigurnost_podataka3 . "' WHERE sID='" . $studentID . "'";
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Sigurnost osobnih podataka</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Molimo Vas da pažljivo pročitate opis pojedinih situacija te da u odgovarajući stupac 
                        ispod <span class="boldtext">Sigurnost osobnih podataka</span> procijenite Vašu situaciju.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext"></h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Sigurnost osobnih podataka</h4></th>
                        </tr>
                        <tr>
                            <th>nikada</th>
                            <th>ne sjećam se</th>
                            <th>u zadnjih 6 mjeseci</th>
                            <th>u prošlih mjesec dana</th>
                            <th>prošli tjedan</th>
                        </tr>
                        <tr>
                            <td>
                                1. Kada si posljednji puta napravio sigurnosnu
                                kopiju (engl. backup) osobnih podataka i
                                dokumenata?
                            </td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="1" data-toggle="tooltip" title="nikada"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="2" data-toggle="tooltip" title="ne sjećam se"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="3" data-toggle="tooltip" title="u zadnjih 6 mjeseci"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="4" data-toggle="tooltip" title="u prošlih mjesec dana"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="5" data-toggle="tooltip" title="prošli tjedan"></td>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="textcentered">više od 10</th>
                            <th class="textcentered">više od 5</th>
                            <th class="textcentered">ja i još 2 osobe</th>
                            <th class="textcentered">ja i još 1 osoba</th>
                            <th class="textcentered">samo ja</th>
                        </tr>
                        <tr>
                            <td>
                                2. Koliko osoba zna zaporku za pristup tvojem sustavu elektroničke pošte?
                            </td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="1" data-toggle="tooltip" title="više od 10"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="2" data-toggle="tooltip" title="više od 5"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="3" data-toggle="tooltip" title="ja i još 2 osobe"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="4" data-toggle="tooltip" title="ja i još 1 osoba"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="5" data-toggle="tooltip" title="samo ja"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Koliko osoba zna zaporku za pristup tvojem Facebooku?
                            </td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka3" value="1" data-toggle="tooltip" title="više od 10"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka3" value="2" data-toggle="tooltip" title="više od 5"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka3" value="3" data-toggle="tooltip" title="ja i još 2 osobe"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka3" value="4" data-toggle="tooltip" title="ja i još 1 osoba"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka3" value="5" data-toggle="tooltip" title="samo ja"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>