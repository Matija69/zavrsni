<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if(isset($_POST['dalje'])) {
            session_start();
            include $_SESSION['konekcija'];
            $spol = $_POST['spol'];
            $dob = $_POST['dob'];
            $razred = $_POST['razred'];
            $ponavljanje = $_POST['ponavljanje'];
            $sql = "INSERT INTO " . $_SESSION['table_name'] . " (spol, dob, razred, ponavljanje) VALUES ('" . $spol . "','" . $dob . "','" . $razred . "','" . $ponavljanje . "')";
            mysqli_query($con, $sql);
            $sessionNum = mysqli_insert_id($con);
            $_SESSION['sid'] = $sessionNum;
            header('Location: ' . next($_SESSION['order']));
        }
    }
    if(!isset($_POST['kreni'])) {
        header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Opći podaci - studenti</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <div class="jumbotron">
                <h3 class="boldtext">ISPITIVANJE PONAŠANJA I ZNANJA KORISNIKA O PITANJIMA KOJA SE TIČU INFORMACIJSKE SIGURNOSTI</h3>
                <p>
                    Ova anketa je namijenjena svim osobama koje koriste računala. Cilj nam je saznati 
                    navike korisnika različitih informacijsko-komunikacijskih računalnih sustava. Anketa je u
                    potpunosti anonimna, molim Vas da iskreno odgovorite na sva pitanja.
                </p>
            </div>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <p class="contentbox">
                    Spol:
                    <br/><br/>
                    <input type="radio" name="spol" value="0"> muško
                    <br/>
                    <input type="radio" name="spol" value="1"> žensko
                </p>
                <p class="contentbox">
                    Koliko imaš godina? 
                    <br/>
                    <input type="number" name="dob" value="15" min="14" max="99" class="textright">
                </p>
                <p class="contentbox">
                    U koji razred ideš:
                    <select name="razred">
                        <option value="0"></option>
                        <option value="1.">Prvi</option>
                        <option value="2.">Drugi</option>
                        <option value="3.">Treći</option>
                        <option value="4.">Četvrti</option>
                    </select>
                </p>
                <p class="contentbox">
                    Jesi li ikad ponavljao/la razred?
                    <br/><br/>
                    <input type="radio" name="ponavljanje" value="0"> Ne
                    <br/>
                    <input type="radio" name="ponavljanje" value="1"> Da
                </p>
                <input type="submit" name="dalje" value="Sljedeći korak >>" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
                $('form').submit(function(e) {
                    var greska = "";
                    var dalje = true;
					if(!$(':radio[name="spol"]:checked').length) {
                        greska += ("Nije odabran spol.\n");
                        dalje = false;
                    }
                    if($('[name="razred"]').val() == 0) { 
						greska += ("Nije odabran razred.\n");
						dalje = false;
					}
					if(!$(':radio[name="ponavljanje"]:checked').length) {
                        greska += ("Nije odabrano ponavljanje razreda.\n");
                        dalje = false;
                    }
                    if(greska.length > 0) {
                        alert(greska);
                        e.preventDefault();
                    }
                    return dalje;
                });   
            });
        </script>
    </body>
</html>