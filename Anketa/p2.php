<?php
    require_once 'init.php';

    function cleanData(&$str) {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

    if(isset($_SESSION['preuzmixls'])) {
        $servername = "localhost";
        $username   = "knenadic";
        $password   = "4nEd2rtU";

        $con = new mysqli($servername, $username, $password);

        if($con->connect_error) {
            die("Neuspjelo spajanje na poslužitelj baze podataka: " . $con->connect_error);
        }
        mysqli_select_db($con, "knenadic");

        $tablename = $_POST['preuzmixls'];
        echo "Table name variable: " . $tablename . "<br>";

        $filename = $tablename . "_" . date('dmY') . ".xls";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        $flag = false;
        $tablenamedb = "";
        $parts = explode(' _ ', $tablename);
        switch ($parts[1]) {
            case 'ucenici':
                $tablenamedb = "ucenici_";
                break;
            case 'studenti':
                $tablenamedb = "studenti_";
                break;
            case 'zaposlenici':
                $tablenamedb = "djelatnici_";
                break;
            default:
                # code...
                break;
        }
        echo "Table start: " . $tablenamedb . "<br>";
        $tablenamedb .= array_search($parts[0], $GLOBALS['config'][6]);
        echo "Whole table name: " . $tablenamedb . "<br>";
        $result = mysqli_query($con, "SELECT * FROM {$tablenamedb}") or die('Query failed!' . $tablenamedb);
        //$result = mysqli_query($con, "SELECT * FROM djelatnici_ekonomska_skola_vukovar") or die('Query failed!' . $tablenamedb);
        // ekonomska_skola_vukovar
        while(($row = mysqli_fetch_assoc($result))) {
            if(!$flag) {
                // display field/column names as first row
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, __NAMESPACE__ . '\cleanData');
            echo implode("\t", array_values($row)) . "\r\n";
        }
    }
    //header('Location: preuzimanje.php');
?>