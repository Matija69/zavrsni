<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            include $_SESSION['konekcija'];          
            $spol       		= $_POST['spol'];
            $dob        		= $_POST['dob'];
            $staz				= $_POST['staz'];
			$sprema				= $_POST['sprema'];
			$broj_zaposlenih 	= $_POST['broj_zaposlenih'];            
            $sql        = "INSERT INTO {$_SESSION['table_name']} (spol, dob, staz, sprema, broj_zaposlenih) VALUES ('" . $spol . "','" . $dob . "','" . $staz . "','" . $sprema . "','" . $broj_zaposlenih . "')";
            mysqli_query($con, $sql);
            $sessionNum = mysqli_insert_id($con);
            $_SESSION['sid'] = $sessionNum;
            header('Location: ' . next($_SESSION['order']));
        } 
    }
    if(!isset($_POST['kreni'])) {
        header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Opći podaci - studenti</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <div class="jumbotron">
                <h3 class="boldtext">ISPITIVANJE PONAŠANJA I ZNANJA KORISNIKA O PITANJIMA KOJA SE TIČU INFORMACIJSKE SIGURNOSTI</h3>
                <p>
                    Ova anketa je namijenjena svim osobama koje koriste računala. Cilj nam je saznati 
                    navike korisnika različitih informacijsko-komunikacijskih računalnih sustava. Anketa je u
                    potpunosti anonimna, molim Vas da iskreno odgovorite na sva pitanja.
                </p>
            </div>
            <form action="ucestalost.php" method="POST">
                <p class="contentbox">
                    Spol:
                    <br/><br/>
                    <input type="radio" name="spol" value="0"> muško
                    <br/>
                    <input type="radio" name="spol" value="1"> žensko
                </p>
                <p class="contentbox">
                    Koliko imate godina? 
                    <br/>
                    <input type="number" name="dob" value="18" min="18" max="99" class="textright">
                </p>
                <p class="contentbox">
					Dužina radnog staža u godinama: 
					<br/>
					<input type="number" name="staz" value="1" min="0" max="99" class="textright">
                </p>
				<p class="contentbox">
					Odaberite stručnu spremu:
					<SELECT name="sprema">
						<option value="0"></option>
						<option value="1">Nekvalificirani (NKV)</option>
						<option value="2">Srednja stručna sprema (SSS)</option>
						<option value="3">Srednja školska sprema (gimnazija, SŠS)</option>
						<option value="4">Viša stručna sprema (VŠS)</option>
						<option value="5">Visoka stručna sprema (VSS)</option>
						<option value="6">magisterrij ili doktorat znanosti</option>
					</SELECT>
				</p>
				<p class="contentbox">
					Odaberite veličinu tvrtke prema broju zaposlenih:
					<SELECT name="broj_zaposlenih">
						<option value="0"></option>
						<option value="1">do 99 zaposlenika</option>
						<option value="2">100 - 499</option>
						<option value="3">500 - 999</option>
						<option value="4">1000 i više</option>
					</SELECT>
				</p>
                <input type="submit" name="dalje" value="Sljedeći korak >>" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
                $('form').submit(function(e) {
                    var greska = "";
                    var dalje = true;
					if(!$(':radio[name="spol"]:checked').length) {
                        greska += ("Nije odabran spol.\n");
                        dalje = false;
                    }
                    if($('[name="sprema"]').val() == 0) {
						greska += ("Nije odabrana stručna sprema.\n");
						dalje = false;
					}
					if($('[name="broj_zaposlenih"]').val() == 0) {
						greska += ("Nije odabran broj zaposlenih.\n");
						dalje = false;
					}
                    if(greska.length > 0) {
                        alert(greska);
                        e.preventDefault();
                    }
                    return dalje;
                });   
            });
        </script>
    </body>
</html>