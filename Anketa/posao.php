<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $posao = array();
            for($i = 1; $i <= 12; $i++) {
                $posao[$i] = $_POST['posao'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 36; $i <= 46; $i++) {
                $sql .= ("p" . $i . "='" . $posao[$i - 35] . "',"); 
            }
            $sql .= ("p47='" . $posao[12] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Ponašanje</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Pred Vama se nalazi niz skala na kojima trebate procijeniti u kojoj mjeri se slažete s dolje
						navedenirn trdnjama, koje se odnose na karakteristike posla koji obavljate. Molimo Vas da na
						skalama koje se nalaze uz svaku tvrdnju zaokruživanjem određenog broja odredite stupanj
						slaganja s predloženim tvrdnjama.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Posao</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Stupanj slaganja</h4></th>
                        </tr>
                        <tr>
                            <th>uopće se NE slažem</th>
                            <th>uglavnom se NE slažem</th>
                            <th>niti se slažem, niti se NE slažem</th>
                            <th>uglavnom se slažem</th>
                            <th>u potpunosti se slažem</th>
                        </tr>
                        <tr>
                            <td>
                                1. U potpunosti imam kontrolu u obavljanju svog posla.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao1" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao1" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao1" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao1" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao1" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Moj posao uključuje više različitih aktivnosti.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao2" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao2" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao2" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao2" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao2" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                3. Samostalno mogu određivati brzinu i ritam obavljanja zadatka.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao3" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao3" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao3" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao3" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao3" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                4. Imam dovoljno materijala i potrebne instrumente (alate) za korektno obavljanje posla.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao4" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao4" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao4" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao4" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao4" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                5. Znam točno koje poslove i dužnosti uključuje moj posao.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao5" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao5" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao5" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao5" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao5" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                6. Imam dovoljno vremena za obavljanje posla u cijelosti.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao6" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao6" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao6" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao6" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao6" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                7. Dobivam suprotne zahtjeve (naloge) od dviju ili više osoba.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao7" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao7" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao7" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao7" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao7" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                8. Moj posao uključuje nebitne i suvišne aktivnosti.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao8" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao8" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao8" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao8" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao8" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                9. Naki poslovi i aktivnosti koje obavljam često nailaze na odobravanje klijenata, a ne nadređenih i obrnuto.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao9" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao9" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao9" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao9" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao9" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                10. Moj posao uključuje prevelik stupanj odgovornosti.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao10" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao10" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao10" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao10" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao10" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                11. Moj posao teškim čine česte nepredvidljive situacije.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao11" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao11" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao11" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao11" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao11" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
						<tr>
                            <td>
                                12. Moj je posao težak.
                            </td>
                            <td class="textcentered"><input type="radio" name="posao12" value="1" data-toggle="tooltip" title="uopće se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao12" value="2" data-toggle="tooltip" title="uglavnom se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao12" value="3" data-toggle="tooltip" title="niti se slažem, niti se NE slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao12" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="posao12" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault(); 
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>