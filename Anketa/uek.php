<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $uek = array();
            for($i = 1; $i <= 15; $i++) {
                $uek[$i] = $_POST['uek'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 83; $i <= 96; $i++) {
                $sql .= ("p" . $i . "='" . $uek[$i - 82] . "',"); 
            }
            $sql .= ("p97='" . $uek[15] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Prvi dojam</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Ovo nije test kojim ispitujemo tvoje znanje i zato ne možetš dati pogrešan odgovor. 
                        Zanima nas kako se obično osjećaš i kako razmišljaš. Na postavljena pitanja odgovaraj 
                        po prvom dojmu i nemoj previše razmišljati o njima.
                    </h4>
                    <h4>
                        Odgovarat ćeš koliko se ponuđene tvrdnje odnose na tebe i to odabirom jedne od 
                        ponuđenih vrijednosti.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Tvrdnja</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Procjena</h4></th>
                        </tr>
                        <tr>
                            <th>uopće ne</th>
                            <th>uglavnom ne</th>
                            <th>kako kada</th>
                            <th>uglavnom da</th>
                            <th>u potpunosti da</th>
                        </tr>
                        <tr>
                            <td>
                                1. Dobro raspoloženje mogu zadržati i ako mi se nešto loše dogodi.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek1" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek1" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek1" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek1" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek1" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Iz neugodnih iskustava učim kako se ubuduće ne treba ponašati.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek2" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek2" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek2" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek2" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek2" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Kod prijatelja mogu razlikovati kada je tužan, a kada razočaran.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek3" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek3" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek3" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek3" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek3" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Lako ću smisliti način da priđem osobi koja mi se sviđa.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek4" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek4" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek4" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek4" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek4" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Lako primijetim promjenu raspoloženja svoga prijatelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek5" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek5" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek5" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek5" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek5" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Lako se mogu domisliti kako obradovati prijatelja kojem idem na rođendan.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek6" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek6" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek6" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek6" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek6" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Lako uvjerim prijatelja da nema razloga za zabrinutost.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek7" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek7" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek7" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek7" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek7" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Mogu dobro izraziti svoje emocije.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek8" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek8" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek8" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek8" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek8" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Mogu opisati kako se osjećam.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek9" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek9" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek9" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek9" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek9" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Mogu reći da znam puno o svom emocionalnom stanju.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek10" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek10" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek10" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek10" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek10" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Nastojim ublažiti neugodne emocije, a pojačati pozitivne.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek11" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek11" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek11" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek11" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek11" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Obaveze ili zadatke radije odmah izvršim, nego da o njima mislim.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek12" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek12" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek12" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek12" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek12" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Primijetim kada netko osjeća krivnju.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek13" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek13" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek13" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek13" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek13" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Većinu svojih osjećaja mogu prepoznati.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek14" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek14" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek14" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek14" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek14" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Znam kako mogu ugodno iznenaditi svakoga svoga prijatelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="uek15" value="1" data-toggle="tooltip" title="uopće ne"></td>
                            <td class="textcentered"><input type="radio" name="uek15" value="2" data-toggle="tooltip" title="uglavnom ne"></td>
                            <td class="textcentered"><input type="radio" name="uek15" value="3" data-toggle="tooltip" title="kako kada"></td>
                            <td class="textcentered"><input type="radio" name="uek15" value="4" data-toggle="tooltip" title="uglavnom da"></td>
                            <td class="textcentered"><input type="radio" name="uek15" value="5" data-toggle="tooltip" title="u potpunosti da"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>