<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            include $_SESSION['konekcija'];            
            $spol       = $_POST['spol'];
            $dob        = $_POST['dob'];
            $fakultet   = $_POST['fakultet'];
            $godina     = $_POST['godina_studija'];
            $ponavljac  = $_POST['ponavljanje'];        
            $sql        = "INSERT INTO {$_SESSION['table_name']} (spol, dob, fakultet, god_stud, ponavljanje) VALUES ('" . $spol . "','" . $dob . "','" . $fakultet . "','" . $godina . "','" . $ponavljac . "')";
            mysqli_query($con, $sql);
            $sessionNum = mysqli_insert_id($con);
            $_SESSION['sid'] = $sessionNum;
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	if(!isset($_POST['kreni'])) {
        header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Opći podaci - studenti</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <div class="jumbotron">
                <h3 class="boldtext">ISPITIVANJE PONAŠANJA I ZNANJA KORISNIKA O PITANJIMA KOJA SE TIČU INFORMACIJSKE SIGURNOSTI</h3>
                <p>
                    Ova anketa je namijenjena svim osobama koje koriste računala. Cilj nam je saznati 
                    navike korisnika različitih informacijsko-komunikacijskih računalnih sustava. Anketa je u
                    potpunosti anonimna, molim Vas da iskreno odgovorite na sva pitanja.
                </p>
            </div>
            <form action="ucestalost.php" method="POST">
                <p class="contentbox">
                    Spol:
                    <br/><br/>
                    <input type="radio" name="spol" value="0"> muško
                    <br/>
                    <input type="radio" name="spol" value="1"> žensko
                </p>
                <p class="contentbox">
                    Koliko imate godina? 
                    <br/>
                    <input type="number" name="dob" value="16" min="18" max="99" class="textright">
                </p>
                <p class="contentbox">
                    Koja ste godina studija?
                    <br/><br/>
                    <input type="radio" name="godina_studija" value="1"> Prva godina preddiplomskog studija
                    <br/>
                    <input type="radio" name="godina_studija" value="2"> Druga godina preddiplomskog studija
                    <br/>
                    <input type="radio" name="godina_studija" value="3"> Treća godina preddiplomskog studija
                    <br/>
                    <input type="radio" name="godina_studija" value="4"> Apsolvent preddiplomskog studija
                    <br/>
                    <input type="radio" name="godina_studija" value="5"> Prva godina diplomskog studija (4. godina studija)
                    <br/>
                    <input type="radio" name="godina_studija" value="6"> Druga godina diplomskog studija (5. godina studija)
                    <br/>
                    <input type="radio" name="godina_studija" value="7"> Apsolvent diplomskog studija
                    <br/>
                </p>
                <p class="contentbox">
                    Jeste li ponavljali koju godinu studiranja?
                    <br/>
                    <input type="radio" name="ponavljanje" value="0"> Ne
                    <br/>
                    <input type="radio" name="ponavljanje" value="1"> Da
                    <br/>
                </p>
                <input type="submit" name="dalje" value="Sljedeći korak >>" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
                $('form').submit(function(e) {
                    var greska = "";
                    var dalje = true;
                    if(!$('[name="spol"]:checked').length) {
                        greska += ("Nije odabran spol.\n");
                        dalje = false;
                    }
                    if($('[name="fakultet"]').val() == "nista") {
                        greska += ("Nije odabran fakultet.\n");
                        dalje = false;
                    }
                    if(!$('[name="godina_studija"]:checked').length) {
                        greska += ("Nije odabrana godina studija.\n");
                        dalje = false;
                    }
                    if(!$('[name="ponavljanje"]:checked').length) {
                        greska += ("Nije odgovoreno pitanje s ponavljanjem godine");
                        dalje = false;
                    }
                    if(greska.length > 0) {
                        alert(greska);
                        e.preventDefault();
                    }
                    return dalje;
                });   
            });
        </script>
    </body>
</html>