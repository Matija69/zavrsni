<?php
	session_start();
    $servername = "localhost";
    $username   = "root";
    $password   = "";

    $con = new mysqli($servername, $username, $password);

    if($con->connect_error) {
        die("Neuspjelo spajanje na poslužitelj baze podataka: " . $con->connect_error);
    }
	mysqli_select_db($con, "anketa");

    $tb_test = mysqli_query($con, "SHOW TABLES LIKE {$_SESSION['table_name']}");
    if(!$tb_test) {
        $sql = "CREATE TABLE {$_SESSION['table_name']} 
                (
                    sID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
                    spol INTEGER UNSIGNED,
                    dob INTEGER UNSIGNED,
					staz INTEGER UNSIGNED,
					sprema INTEGER UNSIGNED,
					broj_zaposlenih INTEGER UNSIGNED,
                    zaporka INTEGER UNSIGNED,
                    p1 INTEGER UNSIGNED,
                    p2 INTEGER UNSIGNED,
                    p3 INTEGER UNSIGNED,
                    p4 INTEGER UNSIGNED,
                    p5 INTEGER UNSIGNED,
                    p6 INTEGER UNSIGNED,
                    p7 INTEGER UNSIGNED,
                    p8 INTEGER UNSIGNED,
                    p9 INTEGER UNSIGNED,
                    p10 INTEGER UNSIGNED,
                    p11 INTEGER UNSIGNED,
                    p12 INTEGER UNSIGNED,
                    p13 INTEGER UNSIGNED,
                    p14 INTEGER UNSIGNED,
                    p15 INTEGER UNSIGNED,
                    p16 INTEGER UNSIGNED,
                    p17 INTEGER UNSIGNED,
                    p18 INTEGER UNSIGNED,
                    p19 INTEGER UNSIGNED,
                    p20 INTEGER UNSIGNED,
                    p21 INTEGER UNSIGNED,
                    p22 INTEGER UNSIGNED,
                    p23 INTEGER UNSIGNED,
                    p24 INTEGER UNSIGNED,
                    p25 INTEGER UNSIGNED,
                    p26 INTEGER UNSIGNED,
                    p27 INTEGER UNSIGNED,
                    p28 INTEGER UNSIGNED,
                    p29 INTEGER UNSIGNED,
                    p30 INTEGER UNSIGNED,
                    p31 INTEGER UNSIGNED,
                    p32 INTEGER UNSIGNED,
                    p33 INTEGER UNSIGNED,
                    p34 INTEGER UNSIGNED,
                    p35 INTEGER UNSIGNED,
                    p36 INTEGER UNSIGNED,
                    p37 INTEGER UNSIGNED,
                    p38 INTEGER UNSIGNED,
                    p39 INTEGER UNSIGNED,
                    p40 INTEGER UNSIGNED,
                    p41 INTEGER UNSIGNED,
                    p42 INTEGER UNSIGNED,
                    p43 INTEGER UNSIGNED,
                    p44 INTEGER UNSIGNED,
                    p45 INTEGER UNSIGNED,
                    p46 INTEGER UNSIGNED,
                    p47 INTEGER UNSIGNED,
                    p48 INTEGER UNSIGNED,
                    p49 INTEGER UNSIGNED,
                    p50 INTEGER UNSIGNED,
                    p51 INTEGER UNSIGNED,
                    p52 INTEGER UNSIGNED,
                    p53 INTEGER UNSIGNED,
                    p54 INTEGER UNSIGNED,
                    p55 INTEGER UNSIGNED,
                    p56 INTEGER UNSIGNED,
                    p57 INTEGER UNSIGNED,
                    p58 INTEGER UNSIGNED,
                    p59 INTEGER UNSIGNED,
                    p60 INTEGER UNSIGNED,
                    p61 INTEGER UNSIGNED,
                    p62 INTEGER UNSIGNED,
                    p63 INTEGER UNSIGNED,
                    p64 INTEGER UNSIGNED,
                    p65 INTEGER UNSIGNED,
                    p66 INTEGER UNSIGNED,
                    p67 INTEGER UNSIGNED,
                    p68 INTEGER UNSIGNED,
                    p69 INTEGER UNSIGNED,
                    p70 INTEGER UNSIGNED,
                    p71 INTEGER UNSIGNED,
                    p72 INTEGER UNSIGNED,
                    p73 INTEGER UNSIGNED,
                    p74 INTEGER UNSIGNED,
                    PRIMARY KEY (sID)
                )
                ENGINE = InnoDB";
        mysqli_query($con, $sql);
    }
?>