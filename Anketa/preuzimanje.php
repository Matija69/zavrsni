<?php
    require_once 'init.php';

    $servername = "localhost";
    $username   = "knenadic";
    $password   = "4nEd2rtU";

    $con = new mysqli($servername, $username, $password);

    if($con->connect_error) {
        die("Neuspjelo spajanje na poslužitelj baze podataka: " . $con->connect_error);
    }
	mysqli_select_db($con, "knenadic");

    function cleanData(&$str) {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

    function preuzmi($tablename) {
        $filename = $tablename . date('dmY') . ".xls";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        $flag = false;
        $result = mysqli_query($con, "SELECT * FROM {$tablename}") or die('Query failed!');
        while(($row = mysqli_fetch_assoc($result))) {
            if(!$flag) {
                // display field/column names as first row
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, __NAMESPACE__ . '\cleanData');
            echo implode("\t", array_values($row)) . "\r\n";
        }
    }

    if (isset($_POST['preuzmixls'])) {
        preuzmi($_POST['preuzmixls']);
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Preuzimanje podataka iz baze</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h2 class="textcentered">Preuzimanje podataka iz baze u .xls datoteke</h2>
            </div>
            <form action="preuzimanje.php" method="post">
                <table class="table table-bordered">
                    <tr>
                        <th><h3>Odaberi tablicu</h3></th>
                    </tr>
                    <?php
                        if(!empty($GLOBALS['config'][3])) {
                            ?>
                            <tr>
                                <td><h4>Učenici</h4></td>
                            </tr>
                            <?php
                            foreach($GLOBALS['config'][3] as $skola) {
                                ?>
                                    <tr>
                                        <td>
                                            <div>
                                                <input type="submit" name="preuzmixls" value="<?php echo 'ucenici_' . $skola ?>" class="btn btn-primary">
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                        if(!empty($GLOBALS['config'][4])) {
                            ?>
                                <tr>
                                    <td><h4>Studenti</h4></td>
                                </tr>
                            <?php
                            foreach($GLOBALS['config'][4] as $fax) {
                                ?>
                                    <tr>
                                        <td>
                                            <div>
                                                <input type="submit" name="preuzmixls" value="<?php echo 'studenti_' . $fax ?>" class="btn btn-primary">
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                        if(!empty($GLOBALS['config'][5])) {
                            ?>
                                <tr>
                                    <td><h4>Zaposlenici</h4></td>
                                </tr>
                            <?php
                            foreach($GLOBALS['config'][5] as $firm) {
                                ?>
                                    <tr>
                                        <td>
                                            <div>
                                                <input type="submit" name="preuzmixls" value="<?php echo 'zaposlenici_' . $firm ?>" class="btn btn-primary">
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                    ?>
                </table>
            </form>
        </div>
    </body>
</html>