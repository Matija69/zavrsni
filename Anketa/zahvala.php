<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Zahvala</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="jumbotron">
                <h3 class="boldtext">Zahvaljujemo na sudjelovanju</h3>
                <p class="j">
                    Zahvaljujemo što ste odvojili dio svog vremena na popunjavanje ove ankete. Podaci će se koristiti za potrebe
                    istraživanja na nacionalnom projektu „Safer Internet Centre Croatia: Making internet a good and safe place” (2015-HR-IA-0013).
                </p>
            </div>
        </div>
    </body>
</html>