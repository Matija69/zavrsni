<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            include $_SESSION['konekcija'];
            $studentID = $_SESSION['sid'];
            $ucestalost = array();
            for($i = 1; $i <= 17; $i++) {
                $ucestalost[$i] = $_POST['ucestalost'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 1; $i <= 16; $i++) {
                $sql .= ("p" . $i . "='" . $ucestalost[$i] . "',"); 
            }
            $sql .= ("p17='" . $ucestalost[17] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Učestalost</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        U sljedećoj tablici opisane su neke situacije koje opisuju uobičajena ponašanja korisnika
                        računalnih informacijsko-komunikacijskih sustava. Molimo Vas da pažljivo pročitate opis
                        pojedinih ponašanja i situacija te da u odgovarajućem stupcu ispod <span class="boldtext">Učestalost</span> odaberete
                        koliko ste se često ponašali na određeni način.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Situacije</h4>Koliko često činite sljedeće?</th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Učestalost</h4></th>
                        </tr>
                        <tr>
                            <th>nikad</th>
                            <th>rijetko</th>
                            <th>ponekad</th>
                            <th>često</th>
                            <th>uvijek</th>
                        </tr>
                        <tr>
                            <td>
                                1. Posuđuješ pristupne podatke (korisničko ime i zaporku/lozinku) kolegama iz razreda 
                                (npr. za vrijeme informatike i slično)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Posuđuješ svojim prijateljima, rođacima, poznanicima svoje privatne pristupne 
                                podatke za pristup kućnome računalu/laptopu/tabletu.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Posuđuješ svojim prijateljima, rođacima, poznanicima svoje privatne 
                                pristupne podatke za pristup svojoj e-mail adresi.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Posuđuješ svojim prijateljima, rođacima, poznanicima svoje ili roditeljske 
                                debitne ili kreditne kartice i pripadajući PIN.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Otkrivaš svoj pin (neskrivanjem, glasnim izgovaranjem prodavaču) 
                                kada plaćaš karticom u trgovini.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Koristiš različite zaporke za različite sustave, npr. za facebook jedna, za mail druga, itd.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Održavaš zaštitu svoga kućnog računala/laptopa/tableta odnosno radiš li nadogradnju 
                                (engl. update) antispayware i antivirusnih programa.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Vršiš nadogradnju i ostalih korisničkih programa te
                                operativnog sustava na tvome kućnome računalu/laptopu/tabletu.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Instaliraš razne programe nepoznatih i manje poznatih
                                proizvođača koji su možda zanimljivi no nisu stvarno 
                                neophodni (npr. razni video playeri, multimedijalni dodaci web preglednicima).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Ostavljaš osobne podatke na društvenim mrežama
                                (npr. privatnu adresu, broj mobitela, poruku da ste na
                                skijanju/ljetovanju i slično).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Odgovaraš na mailove nepoznatih/sumnjivih pošiljatelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Otvaraš, bez provjere, priloge od nepoznatih pošiljatelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Prosljeđuješ/šalješ lančane mailove (npr. poruke o donacijama, sreći i slično).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Koristiš više e-mail adresa (npr. posebno za školu, posebno za facebook).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Prijavljuješ se na e-mail ili facebook s različitih
                                javnih mjesta (npr. internet kafići, razne ustanove,
                                korištenjem besplatne wi-fi mreže i sl.).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Odjavljuješ se sa informacijskog sustava prilikom
                                završetka rada (u školi i kod kuće).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                17. Zaključavaš školsko računalo prilikom kraćeg odlaska
                                iz ureda, učionice, radnog stola na primjer na toalet ili
                                pauzu.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
					$(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {   
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>