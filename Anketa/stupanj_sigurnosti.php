<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $stupanj_sigurnosti = array();
            for($i = 1; $i <= 5; $i++) {
                $stupanj_sigurnosti[$i] = $_POST['stupanj_sigurnosti'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 18; $i <= 21; $i++) {
                $sql .= ("p" . $i . "='" . $stupanj_sigurnosti[$i - 17] . "',"); 
            }
            $sql .= ("p22='" . $stupanj_sigurnosti[5] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Stupanj sigurnosti</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Molimo Vas da pažljivo pročitate opis pojedinih situacija te da u odgovarajući stupac 
                        ispod <span class="boldtext">Stupnja sigurnosti</span> označite koliko su prema Vašem 
                        mišljenju sljedeće situacije sigurne.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Što mislite koliko je sigurno:</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Stupanj sigurnosti</h4></th>
                        </tr>
                        <tr>
                            <th>potpuno nesigurno</th>
                            <th>prilično nesigurno</th>
                            <th>ne znam</th>
                            <th>prilično sigurno</th>
                            <th>potpuno sigurno</th>
                        </tr>
                        <tr>
                            <td>
                                1. Dopisivanje putem elektroničke pošte (e-mail)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti1" value="1" data-toggle="tooltip" title="potpuno nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti1" value="2" data-toggle="tooltip" title="prilično nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti1" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti1" value="4" data-toggle="tooltip" title="prilično sigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti1" value="5" data-toggle="tooltip" title="potpuno sigurno"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Komunikacija putem društvenih mreža (npr. Facebook, Twitter)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti2" value="1" data-toggle="tooltip" title="potpuno nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti2" value="2" data-toggle="tooltip" title="prilično nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti2" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti2" value="4" data-toggle="tooltip" title="prilično sigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti2" value="5" data-toggle="tooltip" title="potpuno sigurno"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Komunikacija mobitelom (razgovori, SMS)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti3" value="1" data-toggle="tooltip" title="potpuno nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti3" value="2" data-toggle="tooltip" title="prilično nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti3" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti3" value="4" data-toggle="tooltip" title="prilično sigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti3" value="5" data-toggle="tooltip" title="potpuno sigurno"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Komunikacija žičnim telefonom
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti4" value="1" data-toggle="tooltip" title="potpuno nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti4" value="2" data-toggle="tooltip" title="prilično nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti4" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti4" value="4" data-toggle="tooltip" title="prilično sigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti4" value="5" data-toggle="tooltip" title="potpuno sigurno"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Općenito komunikacija putem Interneta (npr. Skype, Viber, chat)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti5" value="1" data-toggle="tooltip" title="potpuno nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti5" value="2" data-toggle="tooltip" title="prilično nesigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti5" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti5" value="4" data-toggle="tooltip" title="prilično sigurno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_sigurnosti5" value="5" data-toggle="tooltip" title="potpuno sigurno"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault(); 
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>