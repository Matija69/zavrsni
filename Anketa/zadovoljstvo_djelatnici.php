<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $zadovoljstvo = array();
            for($i = 1; $i <= 17; $i++) {
                $zadovoljstvo[$i] = $_POST['zadovoljstvo'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 48; $i <= 63; $i++) {
                $sql .= ("p" . $i . "='" . $zadovoljstvo[$i - 47] . "',"); 
            }
            $sql .= ("p64='" . $zadovoljstvo[17] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Skala zadovoljstva životom</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        U sljedećoj tablici nalaze se određene tvrdnje koje se odnose na procjenu Vašeg života. Molimo Vas da
                        što iskrenije odgovorite koliko se slažete ili ne slažete sa svakom od njih. Uz svaku tvrdnju odaberite
                        odgovarajući odgovor.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Skala zadovoljstva životom</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Odaberi odgovarajući odgovor</h4></th>
                        </tr>
                        <tr>
                            <th>uopće se <br/> ne slažem</th>
                            <th>uglavnom se <br/> ne slažem</th>
                            <th>niti se slažem <br/> niti se ne slažem</th>
                            <th>uglavnom <br/> se slažem</th>
                            <th>u potpunosti <br/> se slažem</th>
                        </tr>
                        <tr>
                            <td>
                                1. U više aspekata moj život je blizak idealnom.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo1" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo1" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo1" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo1" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo1" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Uvjeti moga života su izvrsni.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo2" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo2" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo2" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo2" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo2" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Zadovoljan sam svojim životom.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo3" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo3" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo3" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo3" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo3" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Do sada imam sve važne stvari koje sam želio u životu.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo4" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo4" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo4" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo4" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo4" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Kada bih ponovno živio svoj život ne bih mijenjao gotovo ništa.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo5" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo5" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo5" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo5" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo5" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Sve u svemu ja sam jako sretna osoba.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo6" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo6" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo6" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo6" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo6" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Život mi donosi puno zadovoljstva.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo7" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo7" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo7" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo7" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo7" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Ja se općenito dobro osjećam.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo8" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo8" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo8" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo8" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo8" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Mislim da sam sretna osoba.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo9" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo9" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo9" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo9" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo9" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                10. U cjelini gledajući ja sam manje sretan od drugih ljudi.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo10" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo10" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo10" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo10" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo10" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Često sam utučen i žalostan.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo11" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo11" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo11" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo11" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo11" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Mislim da sam sretan barem koliko i drugi ljudi.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo12" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo12" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo12" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo12" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo12" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Moja budućnost izgleda dobro.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo13" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo13" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo13" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo13" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo13" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Zadovoljan sam načinom na koji mi se ostvaruju planovi.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo14" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo14" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo14" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo14" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo14" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Što god da se dogodi mogu vidjeti i svjetlu stranu.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo15" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo15" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo15" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo15" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo15" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Uživam živjeti.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo16" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo16" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo16" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo16" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo16" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                17. Moj mi se život čini smislen.
                            </td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo17" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo17" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo17" value="3" data-toggle="tooltip" title="niti se slažem niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo17" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="zadovoljstvo17" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>