<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include 'konekcija.php';
            $stupanj_sigurnosti = array();
            for($i = 1; $i <= 5; $i++) {
                $stupanj_sigurnosti[$i] = $_POST['stupanj_sigurnosti'.$i];
            }
            $sql = "UPDATE anketa.unios_studenti SET ";
            for($i = 18; $i <= 21; $i++) {
                $sql .= ("p" . $i . "='" . $stupanj_sigurnosti[$i - 17] . "',"); 
            }
            $sql .= ("p22='" . $stupanj_sigurnosti[5] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
        } 
    } else {
        header('Location: index.html');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Stupanj uvjerenja</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="stupanj_vaznosti.php" method="POST">
                <div class="contentbox">
                    <h4>
                        Molimo Vas da pažljivo pročitate opis pojedinih situacija te da u odgovarajući stupac
                        ispod <span class="boldtext">Stupnja uvjerenja</span> označite koliko ste uvjereni da 
                        će vam se dogoditi sljedeće situacije.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Koliko ste uvjereni da postoji realna opasnost:</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Stupanj uvjerenja</h4></th>
                        </tr>
                        <tr>
                            <th>nisam uvjeren/a</th>
                            <th>možda</th>
                            <th>ne znam</th>
                            <th>prilično</th>
                            <th>potpuno</th>
                        </tr>
                        <tr>
                            <td>
                                6. Da će vam netko ukrasti službene podatke sa službenog računala (u firmi ili na fakultetu)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja1" value="1" data-toggle="tooltip" title="nisam uvjeren/a"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja1" value="2" data-toggle="tooltip" title="možda"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja1" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja1" value="4" data-toggle="tooltip" title="prilično"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja1" value="5" data-toggle="tooltip" title="potpuno"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Da će vam netko ukrasti privatne podatke s vašeg kućnog računala
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja2" value="1" data-toggle="tooltip" title="nisam uvjeren/a"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja2" value="2" data-toggle="tooltip" title="možda"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja2" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja2" value="4" data-toggle="tooltip" title="prilično"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja2" value="5" data-toggle="tooltip" title="potpuno"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Da će vam netko ukrasti privatne podatke s vašeg mobilnog uređaja
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja3" value="1" data-toggle="tooltip" title="nisam uvjeren/a"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja3" value="2" data-toggle="tooltip" title="možda"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja3" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja3" value="4" data-toggle="tooltip" title="prilično"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja3" value="5" data-toggle="tooltip" title="potpuno"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Da će vam netko otuđiti novac s vašeg računa u banci
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja4" value="1" data-toggle="tooltip" title="nisam uvjeren/a"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja4" value="2" data-toggle="tooltip" title="možda"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja4" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja4" value="4" data-toggle="tooltip" title="prilično"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja4" value="5" data-toggle="tooltip" title="potpuno"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Da će vam netko ukrasti identitet na Internetu (e-banking, facebook, e-mail)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja5" value="1" data-toggle="tooltip" title="nisam uvjeren/a"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja5" value="2" data-toggle="tooltip" title="možda"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja5" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja5" value="4" data-toggle="tooltip" title="prilično"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_uvjerenja5" value="5" data-toggle="tooltip" title="potpuno"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault(); 
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>