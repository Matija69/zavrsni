<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            // stupanj_uvjerenja
            session_start();
            $studentID = $_SESSION['sid'];
            include 'konekcija.php';
            $stupanj_uvjerenja = array();
            for($i = 1; $i <= 5; $i++) {
                $stupanj_uvjerenja[$i] = $_POST['stupanj_uvjerenja'.$i];
            }
            $sql = "UPDATE anketa.unios_studenti SET ";
            for($i = 23; $i <= 26; $i++) {
                $sql .= ("p" . $i . "='" . $stupanj_uvjerenja[$i - 22] . "',"); 
            }
            $sql .= ("p27='" . $stupanj_uvjerenja[5] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
        } 
    } else {
        header('Location: index.html');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Stupanj važnosti</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="sigurnost_osobnih_podataka.php" method="POST">
                <div class="contentbox">
                    <h4>
                        Molimo Vas da pažljivo pročitate opis pojedinih situacija te da u odgovarajući stupac
                        ispod <span class="boldtext">Stupnja važnosti</span> označite koliko je prema Vašem mišljenju 
                        važno raditi sljedeće stvari.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Prema vašem mišljenju koliko je važno</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Stupanj važnosti</h4></th>
                        </tr>
                        <tr>
                            <th>potpuno nevažno</th>
                            <th>prilično nevažno</th>
                            <th>ne znam</th>
                            <th>prilično važno</th>
                            <th>izrazito važno</th>
                        </tr>
                        <tr>
                            <td>
                                11. Kopirati važnije dokumente na još jednu, drugu lokaciju odnosno drugi memorijski 
                                uređaj (izrada pričuvnih kopija podataka)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Provjeriti tuđi USB memorijski štapić (stick) od virusa prije učitavanja podataka
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Bezuvjetno čuvati tajnost svojih zaporki (lozinki)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Periodično zamijeniti svoje zaporke novima, barem za važnije sustave
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Odvajati poslovne računalne resurse od privatnih (npr. prijenosna memorija, 
                                elektronička pošta, telefon, …)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Čuvati od krađe svoj USB memorijski štapić s važnim podacima
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) { 
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>