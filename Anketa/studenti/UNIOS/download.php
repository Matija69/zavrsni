<?php 

	// Original PHP code by Chirp Internet: www.chirp.com.au
	
	include 'konekcija.php';
	
	function cleanData(&$str) {
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	}
	
	$filename = "unios_studenti_" . date('dmY') . ".xls";
	
	header("Content-Disposition: attachment; filename=\"$filename\"");
	header("Content-Type: application/vnd.ms-excel");

	$flag = false;
	$result = mysqli_query($con, "SELECT * FROM unios_studenti") or die('Query failed!');
	while(($row = mysqli_fetch_assoc($result))) {
		if(!$flag) {
			// display field/column names as first row
			echo implode("\t", array_keys($row)) . "\r\n";
			$flag = true;
		}
		array_walk($row, __NAMESPACE__ . '\cleanData');
		echo implode("\t", array_values($row)) . "\r\n";
	}
	exit;
?>
