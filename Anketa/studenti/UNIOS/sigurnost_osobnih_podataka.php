<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include 'konekcija.php';
            $stupanj_vaznosti = array();
            for($i = 1; $i <= 6; $i++) {
                $stupanj_vaznosti[$i] = $_POST['stupanj_vaznosti'.$i];
            }
            $sql = "UPDATE anketa.unios_studenti SET ";
            for($i = 28; $i <= 32; $i++) {
                $sql .= ("p" . $i . "='" . $stupanj_vaznosti[$i - 27] . "',"); 
            }
            $sql .= ("p33='" . $stupanj_vaznosti[6] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
        } 
    } else {
        header('Location: index.html');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Sigurnost osobnih podataka</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="ponasanja.php" method="POST">
                <div class="contentbox">
                    <h4>
                        Molimo Vas da pažljivo pročitate opis pojedinih situacija te da u odgovarajući stupac 
                        ispod <span class="boldtext">Sigurnost osobnih podataka</span> procijenite Vašu situaciju.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext"></h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Sigurnost osobnih podataka</h4></th>
                        </tr>
                        <tr>
                            <th>nikada</th>
                            <th>ne sjećam se</th>
                            <th>u zadnjih 6 mjeseci</th>
                            <th>u prošlih mjesec dana</th>
                            <th>prošli tjedan</th>
                        </tr>
                        <tr>
                            <td>
                                1. Kada ste posljednji puta radili sigurnosnu kopiju (engl. backup) osobnih podataka i dokumenata?
                            </td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="1" data-toggle="tooltip" title="nikada"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="2" data-toggle="tooltip" title="ne sjećam se"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="3" data-toggle="tooltip" title="u zadnjih 6 mjeseci"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="4" data-toggle="tooltip" title="u prošlih mjesec dana"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka1" value="5" data-toggle="tooltip" title="prošli tjedan"></td>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="textcentered">više od 10</th>
                            <th class="textcentered">više od 5</th>
                            <th class="textcentered">ja i još 2 osobe</th>
                            <th class="textcentered">ja i još 1 osoba</th>
                            <th class="textcentered">samo ja</th>
                        </tr>
                        <tr>
                            <td>
                                2. Koliko osoba zna zaporku za pristup vašem sustavu elektroničke pošte?
                            </td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="1" data-toggle="tooltip" title="više od 10"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="2" data-toggle="tooltip" title="više od 5"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="3" data-toggle="tooltip" title="ja i još 2 osobe"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="4" data-toggle="tooltip" title="ja i još 1 osoba"></td>
                            <td class="textcentered"><input type="radio" name="sigurnost_podataka2" value="5" data-toggle="tooltip" title="samo ja"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>