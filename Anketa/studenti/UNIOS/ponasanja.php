<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include 'konekcija.php';
            $sigurnost_podataka1 = $_POST['sigurnost_podataka1'];
            $sigurnost_podataka2 = $_POST['sigurnost_podataka2'];
            $sql = "UPDATE anketa.unios_studenti SET p34='" . $sigurnost_podataka1 . "', p35='" . $sigurnost_podataka2 . "' WHERE sID='" . $studentID . "'";
            mysqli_query($con, $sql);
        } 
    } else {
        header('Location: index.html');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Ponašanje</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="zadovoljstvo.php" method="POST">
                <div class="contentbox">
                    <h4>
                        U sljedećoj tablici opisana su neka ponašanja koja čine mladi. Molimo Te da pažljivo
                        pročitaš opis pojedinih ponašanja i da u odgovarajući stupac ispod <span class="boldtext">Broja puta</span> odabereš
                        koliko si se puta ponašao na određeni način.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Ponašanja</h4>Koliko si puta u životu</th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Broj puta</h4></th>
                        </tr>
                        <tr>
                            <th>0</th>
                            <th>1-4</th>
                            <th>5-10</th>
                            <th>11-20</th>
                            <th>21 i više</th>
                        </tr>
                        <tr>
                            <td>
                                1. Vozio/la skuter ili motor iako nisi položio/la vozački ispit.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje1" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje1" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje1" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje1" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje1" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Vozio/la automobil iako nisi položio/la vozački ispit.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje2" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje2" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje2" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje2" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje2" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Nasilno ušao (provalio) u školu, trgovinu, tuđi stan kako bi uzeo/la novac ili vrijedne stvari.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje3" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje3" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje3" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje3" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje3" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Iskoristio priliku (npr. otvoren prozor) i ušao u školu, 
                                trgovinu, tuđi stan kako bi uzeo novac ili vrijedne stvari.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje4" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje4" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje4" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje4" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje4" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Nasilno otvorio (provalio) zaključani automobil kako bi uzeo novac ili vrijedne stvari.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje5" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje5" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje5" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje5" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje5" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Skidao/la dijelove s tuđeg automobila, motora ili 
                                bicikla (kao npr. brisače, svjetla i sl.) i prisvojio/la ih.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje6" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje6" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje6" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje6" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje6" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Uzeo/la u samoposluzi ili nekoj drugoj trgovini stvar ili 
                                stvari vrijedne više od 100 KN, a da nisi platio/la.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje7" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje7" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje7" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje7" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje7" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Uzeo/la tuđi automobil bez dozvole vlasnika, da se njime provozaš.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje8" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje8" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje8" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje8" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje8" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Uzeo/la tuđi motor ili skuter, bez dozvole vlasnika, da se njime provozaš.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje9" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje9" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje9" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje9" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje9" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Uzeo/la novac iz nečije torbe ili novčanika bez znanja vlasnika.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje10" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje10" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje10" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje10" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje10" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Uzeo/la bicikl koji ti ne pripada, bez namjere da ga vratiš vlasniku.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje11" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje11" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje11" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje11" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje11" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Prisvojio/la u školi, odgojnoj ustanovi ili kući 
                                prijatelja, poznanika ili rođaka stvari ili novac koji ti nisu pripadali.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje12" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje12" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje12" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje12" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje12" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Prijetio/la batinama nekoj osobi kako bi došao/la do novca ili vrjednijih stvari.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje13" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje13" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje13" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje13" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje13" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Udario/la ili ozbiljnije ozlijedio/la nekoga kako bi došao/la do novca ili vrjednijih stvari.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje14" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje14" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje14" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje14" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje14" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Namjerno razbio/la prozor, uličnu svjetiljku, izlog, koševe za smeće, klupu ili slično.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje15" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje15" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje15" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje15" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje15" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Ispustio/la gume na automobilu, ili ga na neki drugi 
                                način oštetio/la kao npr. izgrebao/la lak, razbio/la prozor i sl..
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje16" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje16" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje16" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje16" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje16" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                17. Namjerno uništio/la školski namještaj, stolice u kinu ili tramvaju, autobusu i sl..
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje17" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje17" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje17" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje17" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje17" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                18. Crtao/la grafite <span class="boldtext">bez službene dozvole</span> (npr. vlasnika 
                                kuće, stanara zgrade, vlasti za npr. školu, tramvajske stanice, vlakove…)
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje18" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje18" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje18" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje18" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje18" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                19. Oštetio/la automat u kojima se nalazi novac (npr. 
                                automat za parkiranje, automate s napicima i sl.).
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje19" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje19" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje19" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje19" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje19" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                20. Opio/la pivom ili vinom.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje20" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje20" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje20" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje20" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje20" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                21. Opio/la <span class="boldtext">oštrim alkoholnim pićima</span> kao npr. votka, viski i sl.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje21" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje21" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje21" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje21" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje21" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                22. Bio/la u školi pod utjecajem alkohola, odnosno, pijan/a.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje22" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje22" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje22" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje22" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje22" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                23. "Snifao/la" ili "gongao/la" se (udisao/la ljepilo).
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje23" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje23" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje23" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje23" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje23" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                24. Pušio/la marihuanu ili hašiš.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje24" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje24" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje24" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje24" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje24" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                25. Koristio/la ecstasy ("bombone"), "ice" i slične droge.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje25" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje25" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje25" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje25" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje25" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                26. Koristio/la neku tešku drogu kao npr. kokain, heroin, LSD, speed i sl.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje26" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje26" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje26" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje26" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje26" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                27. Bio/la u školi pod utjecajem neke droge.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje27" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje27" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje27" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje27" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje27" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                28. Preprodavao/la hašiš ili marihuanu.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje28" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje28" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje28" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje28" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje28" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                29. Preprodavao/la kokain, heroin ili LSD.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje29" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje29" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje29" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje29" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje29" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                30. Prenoćio/la van kuće bez dopuštenja ili znanja roditelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje30" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje30" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje30" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje30" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje30" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                31. Pobjegao/la od kuće.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje31" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje31" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje31" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje31" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje31" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                32. Sudjelovao/la u krađi ili nekoj drugoj krivičnoj 
                                aktivnosti koju su predvodili neki tvoji prijatelji.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje32" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje32" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje32" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje32" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje32" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                33. Primio/la ili kupio/la nešto za što si znao/la da je ukradeno (npr. mobitel, sat i sl.).
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje33" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje33" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje33" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje33" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje33" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                34. Uzeo/la građevinski materijal ili druge stvari s gradilišta ili iz skladišta.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje34" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje34" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje34" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje34" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje34" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                35. Krivotvorio/la novčanice ili službene dokumente, npr. svjedodžba, 
                                osobna iskaznica, putovnica, automobilske tablice i sl.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje35" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje35" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje35" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje35" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje35" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                36. Razbijao/la stvari po kući kad su ti roditelji ili drugi 
                                odrasli članovi obitelji nešto zabranili ili ti nisu dali
                                novac koji si tražio/la.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje36" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje36" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje36" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje36" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje36" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                37. Udario/la majku, oca ili drugog odraslog člana obitelji
                                kad su ti nešto zabranili (izlazak, vožnju mopedom i
                                sl.) ili ti nisu dali novac koji si tražio/la.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje37" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje37" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje37" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje37" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje37" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                38. Udario/la ili ozbiljnije ozlijedio/la nastavnike, profesore ili odgajatelje (u domu)
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje38" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje38" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje38" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje38" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje38" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                39. Udario/la ili ozbiljnije ozlijedio/la službene osobe (npr. 
                                policajce, suce, kontrolore u tramvaju).
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje39" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje39" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje39" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje39" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje39" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                40. Bio/la nasilan/na prema životinja (udarao/la ih, 
                                bacao/la kamenje na njih, bacao/la životinje s visine, 
                                na različite načine ih ozljeđivao/la).
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje40" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje40" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje40" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje40" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje40" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                41. Bio/la fizički nasilan/na prema svojoj djevojci/mladiću.
                            </td>
                            <td class="textcentered"><input type="radio" name="ponasanje41" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje41" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje41" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje41" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje41" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                42. Sudjelovao/la u nasilju na utakmicama (npr. palio/la 
                                baklje, tukao/la se, kidao/la stolce i sl.).
                            <td class="textcentered"><input type="radio" name="ponasanje42" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje42" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje42" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje42" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje42" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                43. Sudjelovao/la u napadu na osobu zbog njene vjere, rase
                                ili spolne orijentacije (npr. Romi, homoseksualci, transvestiti).
                            <td class="textcentered"><input type="radio" name="ponasanje43" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje43" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje43" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje43" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje43" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                        <tr>
                            <td>
                                44. Preprodavao/la presnimljeni audiovizualni materijal (albume, filmove i slično).
                            <td class="textcentered"><input type="radio" name="ponasanje44" value="1" data-toggle="tooltip" title="0"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje44" value="2" data-toggle="tooltip" title="1 - 4"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje44" value="3" data-toggle="tooltip" title="5 - 10"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje44" value="4" data-toggle="tooltip" title="11 - 20"></td>
                            <td class="textcentered"><input type="radio" name="ponasanje44" value="5" data-toggle="tooltip" title="21 i više"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault(); 
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>