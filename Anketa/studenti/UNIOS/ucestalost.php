<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            include 'konekcija.php';
            
            $spol       = $_POST['spol'];
            $dob        = $_POST['dob'];
            $fakultet   = $_POST['fakultet'];
            $godina     = $_POST['godina_studija'];
            $ponavljac  = $_POST['ponavljanje'];
            $zaporka    = $_POST['zaporka'];
            
            if(strlen($zaporka) > 0) {
                $pwd = 1;
            } else {
                $pwd = 0;
            }
            $sql        = "INSERT INTO anketa.unios_studenti (spol, dob, fakultet, god_stud, ponavljanje, zaporka) VALUES ('" . $spol . "','" . $dob . "','" . $fakultet . "','" . $godina . "','" . $ponavljac . "','" . $pwd . "')";
            mysqli_query($con, $sql);
            $sessionNum = mysqli_insert_id($con);
            $_SESSION['sid'] = $sessionNum;
        } 
    } else {
        header('Location: index.html');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Učestalost</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="stupanj_sigurnosti.php" method="POST">
                <div class="contentbox">
                    <h4>
                        U sljedećoj tablici opisane su neke situacije koje opisuju uobičajena ponašanja korisnika
                        računalnih informacijsko-komunikacijskih sustava. Molimo Vas da pažljivo pročitate opis
                        pojedinih ponašanja i situacija te da u odgovarajućem stupcu ispod <span class="boldtext">Učestalost</span> odaberete
                        koliko ste se često ponašali na određeni način.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Situacije</h4>Koliko često činite sljedeće?</th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Učestalost</h4></th>
                        </tr>
                        <tr>
                            <th>nikad</th>
                            <th>rijetko</th>
                            <th>ponekad</th>
                            <th>često</th>
                            <th>uvijek</th>
                        </tr>
                        <tr>
                            <td>
                                1. Posuđujete službene pristupne podatke (korisničko ime i zaporka) kolegama
                                studentima ili na poslu, koji se nađu u potrebi (npr. za vrijeme bolovanja,
                                godišnjeg)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost1" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Posuđujete svojim prijateljima, rođacima, poznanicima svoje privatne pristupne
                                podatke za pristup kućnome računalu.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost2" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Posuđujete svojim prijateljima, rođacima, poznanicima svoje privatne pristupne
                                podatke za pristup osobnoj/privatnoj adresi elemtroničke pošte (e-mail).
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost3" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Posuđujete svojim prijateljima, rođacima, poznanicima svoje privatne kreditne kartice i pripadajući PIN.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost4" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Otkrivate svoj PIN (neskrivanjem, glasnim izgovaranjem prodavaču) kada plaćate karticom u trgovini.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost5" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Koristite različite zaporke za različite sustave, npr. za facebook jedna, za e-mail druga, za poslovni sustav treća zaporka (lozinka), itd.
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost6" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Održavate zaštitu svoga privatnog računala odnosno radite li nadogradnju (engl. update) antispayware i antivirusnih programa
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost7" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Vršite nadogradnju i ostalih korisničkih programa te operativnog sustava na vašem privatnom računalu
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost8" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Instalirate razne programe nepoznatih i manje poznatih proizvođača koji su možda
                                zanimljivi no nisu stvarno neophodni (npr. razni video playeri, multimedijalni dodaci web preglednicima)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost9" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Ostavljate osobne podatke na društvenim mrežama (npr. privatnu adresu, broj mobitela, poruku da ste na godišnjem i sl.)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost10" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Odgovarate na e-mailove nepoznatih/sumnjivih pošiljatelja
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost11" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Otvarate, bez provjere, priloge nepoznatih pošiljatelja
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost12" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Prosljeđujete/šaljete lančane e-mailove (npr. poruke o donacijama, sreći i sl.)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost13" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Koristite više e-mail adresa (npr. privatni i službeni e-mail)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost14" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Prijavljujete se na vaš e-mail s različitih javnih mjesta (npr. internet kafići, razne 
                                ustanove, korištenjem besplatne wi-fi mreže i sl.)
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost15" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Odjavljujete se sa informacijskog sustava prilikom završetka rada
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost16" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                        <tr>
                            <td>
                                17. Zaključavate službeno računalo prilikom kraćeg odlaska iz ureda, učionice, radnog stola na primjer na toalet ili pauzu
                            </td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="2" data-toggle="tooltip" title="rijetko (nekoliko puta godišnje)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="3" data-toggle="tooltip" title="ponekad (nekoliko puta mjesečno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="4" data-toggle="tooltip" title="često (nekoliko puta tjedno)"></td>
                            <td class="textcentered"><input type="radio" name="ucestalost17" value="5" data-toggle="tooltip" title="uvijek (skoro svaki dan)"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {   
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>