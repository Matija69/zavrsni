<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['kraj'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include 'konekcija.php';
            $stres = array();
            for($i = 1; $i <= 10; $i++) {
                $stres[$i] = $_POST['stres'.$i];
            }
            $sql = "UPDATE anketa.unios_studenti SET ";
            for($i = 97; $i <= 105; $i++) {
                $sql .= ("p" . $i . "='" . $stres[$i - 96] . "',"); 
            }
            $sql .= ("p106='" . $stres[10] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
        } 
    } else {
        header('Location: index.html');
    }
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Skala stresa</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="jumbotron">
                <h3 class="boldtext">Zahvaljujemo na sudjelovanju</h3>
                <p class="j">
                    Zahvaljujemo što ste odvojili dio svog vremena na popunjavanje ove ankete. Podaci će se koristiti za potrebe
                    istraživanja na nacionalnom projektu „Safer Internet Centre Croatia: Making internet a good and safe place” (2015-HR-IA-0013).
                </p>
            </div>
        </div>
    </body>
</html>