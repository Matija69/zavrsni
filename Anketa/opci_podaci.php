<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $siblinzi 			= $_POST['siblinzi'];
            $suzivot 			= $_POST['suzivot'];
            $prijatelji 			= $_POST['prijatelji'];
            $materijalno 		= $_POST['materijalno'];
            $polugodiste 		= $_POST['polugodiste'];
            $prosla_godina 	= $_POST['prosla_godina'];
            $sql = "UPDATE {$_SESSION['table_name']} SET siblinzi = {$siblinzi}, suzivot = {$suzivot}, prijatelji = {$prijatelji}, materijalno = {$materijalno}, polugodiste = {$polugodiste}, prosla_godina = {$prosla_godina} WHERE sID = {$studentID}";
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Opći podaci - učenici</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <div class="jumbotron">
                <h3 class="boldtext">ISPITIVANJE PONAŠANJA I ZNANJA KORISNIKA O PITANJIMA KOJA SE TIČU INFORMACIJSKE SIGURNOSTI</h3>
                <p>
                    Molim te da za kraj odgovoriš na još nekoliko pitanja o sebi
                </p>
            </div>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <p class="contentbox">
                    Koliko braće i sestara imaš (uključujući i polubraću/polusestre)?
                    <br/><br/>
                    <input type="radio" name="siblinzi" value="0"> 0
                    <br/>
                    <input type="radio" name="siblinzi" value="1"> 1
                    <br/>
                    <input type="radio" name="siblinzi" value="2"> 2
                    <br/>
                    <input type="radio" name="siblinzi" value="3"> 3
                    <br/>
                    <input type="radio" name="siblinzi" value="4"> 4 ili više
                </p>
                <p class="contentbox">
                    S kim živiš?
                    <br/><br/>
                    <input type="radio" name="suzivot" value="0"> S oba roditelja
                    <br/>
                    <input type="radio" name="suzivot" value="1"> S ocem
                    <br/>
                    <input type="radio" name="suzivot" value="2"> S majkom
                    <br/>
                    <input type="radio" name="suzivot" value="3"> S nekim drugim
                </p>
                <p class="contentbox">
                    Koliko imaš najboljih prijatelja?
                    <br/><br/>
                    <input type="radio" name="prijatelji" value="0"> 0
                    <br/>
                    <input type="radio" name="prijatelji" value="1"> 1
                    <br/>
                    <input type="radio" name="prijatelji" value="2"> 2
                    <br/>
                    <input type="radio" name="prijatelji" value="3"> 3
                    <br/>
                    <input type="radio" name="prijatelji" value="4"> 4 ili više
                </p>
                <p class="contentbox">
                    Kako bi procijenio/la materijalno stanje tvoje obitelji?
                    <br/><br/>
                    <input type="radio" name="materijalno" value="0"> Jedva "spajamo kraj s krajem" iz mjeseca u mjesec.
                    <br/>
                    <input type="radio" name="materijalno" value="1"> Imamo dovoljno novca za podmirenje svojih osnovnih troškova.
                    <br/>
                    <input type="radio" name="materijalno" value="2"> Imamo dovoljno novca za podmirenje troškova te uspijemo nešto i uštedjeti.
                    <br/>
                    <input type="radio" name="materijalno" value="3"> Ugodno možemo živjeti ne brinući se zbog novca.
                </p>
                <p class="contentbox">
                    Polugodište sam završio/la s:
                    <br/><br/>
                    <input type="radio" name="polugodiste" value="0"> Izvrsnim uspjehom (5)
                    <br/>
                    <input type="radio" name="polugodiste" value="1"> Vrlo dobrim uspjehom (4)
                    <br/>
                    <input type="radio" name="polugodiste" value="2"> Dobrim uspjehom (3)
                    <br/>
                    <input type="radio" name="polugodiste" value="3"> Dovoljnim uspjehom (2)
                    <br/>
                    <input type="radio" name="polugodiste" value="4"> Pao sam
                </p>
                <p class="contentbox">
                    Prošlu školsku godinu sam zvaršio/la s:
                    <br/><br/>
                    <input type="radio" name="prosla_godina" value="0"> Izvrsnim uspjehom (5)
                    <br/>
                    <input type="radio" name="prosla_godina" value="1"> Vrlo dobrim uspjehom (4)
                    <br/>
                    <input type="radio" name="prosla_godina" value="2"> Dobrim uspjehom (3)
                    <br/>
                    <input type="radio" name="prosla_godina" value="3"> Dovoljnim uspjehom (2)
                    <br/>
                    <input type="radio" name="prosla_godina" value="4"> Pao sam
                </p>
                <input type="submit" name="kraj" value="Kraj ankete" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>