<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $ioskala = array();
            for($i = 1; $i <= 29; $i++) {
                $ioskala[$i] = $_POST['ioskala'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 98; $i <= 125; $i++) {
                $sql .= ("p" . $i . "='" . $ioskala[$i - 97] . "',"); 
            }
            $sql .= ("p126='" . $ioskala[29] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Prvi dojam</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        U nastavku Upitnika nalazi se niz tvrdnji, a uz svaku tvrdnju nalazi se i skala 
                        od pet stupnjeva. Na skali zaokružite odgovarajući broj koji će označiti koliko 
                        se navedene tvrdnje odnose na Vas.
                    </h4>
                    <h4>
                        Odgovarat ćeš koliko se ponuđene tvrdnje odnose na tebe i to odabirom jedne od 
                        ponuđenih vrijednosti.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th class="textcentered"><h4 class="boldtext">Tvrdnja</h4></th>
                            <th>uopće se <br>ne slažem</th>
                            <th>uglavnom se <br>ne slažem</th>
                            <th>niti se slažem, <br>niti se ne slažem</th>
                            <th>uglavnom se <br>slažem</th>
                            <th>u potpunosti <br>se slažem</th>
                        </tr>
                        <tr>
                            <td>
                                1. Za mene je važno da uvijek mogu biti s drugim ljudima.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala1" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala1" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala1" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala1" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala1" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                2. U kontaktu s ljudima unosim cijelog sebe.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala2" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala2" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala2" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala2" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala2" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Ljudi su surovi i nemam dovoljno povjerenja u njih.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala3" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala3" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala3" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala3" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala3" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Čovjekovo vrijeme je ionako ograničeno, a drugi ljudi
                                mu ga pokušavaju još više oduzeti.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala4" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala4" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala4" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala4" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala4" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                5. U potpunosti se predajem svojim prijateljima.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala5" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala5" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala5" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala5" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala5" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Moje najveće bogatstvo je u brojnim prijateljstvima.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala6" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala6" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala6" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala6" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala6" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Često posjećujem mjesta na kojima ima puno ljudi.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala7" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala7" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala7" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala7" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala7" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Ljudi uvijek u presudnom trenutku okrenu leđa.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala8" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala8" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala8" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala8" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala8" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Sa svakim želim biti prijatelj.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala9" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala9" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala9" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala9" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala9" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Bez obzira šta radim radije to radim surađujući s drugim ljudima.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala10" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala10" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala10" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala10" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala10" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Život s drugim ljudima je vječita borba.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala11" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala11" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala11" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala11" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala11" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Ljudi su mi toliko potrebni da čak i kad sam sam
                                moram uključiti radio da bih čuo ljudski glas.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala12" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala12" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala12" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala12" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala12" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Čovjek je čovjeku vuk.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala13" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala13" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala13" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala13" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala13" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                14. U mom životu ljudi su mi najvažniji.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala14" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala14" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala14" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala14" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala14" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Što više ljudi upoznajem to mi je teže sačuvati mir i
                                životno zadovoljstvo.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala15" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala15" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala15" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala15" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala15" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Društvo drugih ljudi smatram nužnim zlom i trudim se da ga izbjegavam.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala16" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala16" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala16" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala16" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala16" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                17. Ljudi su najbolji lijek za moju tugu.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala17" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala17" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala17" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala17" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala17" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                18. Ljudi su po prirodi zli.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala18" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala18" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala18" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala18" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala18" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                19. Sve što u životu proživljavam moram podijeliti s nekim.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala19" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala19" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala19" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala19" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala19" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                20. Najbolje se osjećam kad sam u društvu prijatelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala20" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala20" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala20" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala20" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala20" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                21. Mislim da je druženje često puta rasipanje dragocjenog vremena.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala21" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala21" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala21" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala21" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala21" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                22. Najsretniji sam kad sam s drugim ljudima.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala22" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala22" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala22" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala22" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala22" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                23. Moja najsretnija iskustva vezana su uz ljude.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala23" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala23" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala23" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala23" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala23" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                24. Često poželim da se nisam ni rodio, samo zbog ljudi oko mene.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala24" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala24" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala24" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala24" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala24" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                25. Društvo drugih ljudi najviše mi pomaže kad sam nesretan.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala25" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala25" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala25" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala25" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala25" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                26. Najljepše se osjećam kad sam sam (sama).
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala26" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala26" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala26" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala26" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala26" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                27. Moja potreba za ljudima je vrlo mala.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala27" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala27" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala27" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala27" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala27" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                28. Često odlazim u kino da bih bio među ljudima.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala28" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala28" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala28" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala28" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala28" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                29. Sretan sam kad pronađem neki miran kutak 
                                gdje me drugi ljudi ne mogu uznemiravati.
                            </td>
                            <td class="textcentered"><input type="radio" name="ioskala29" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala29" value="2" data-toggle="tooltip" title="uglavnom se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala29" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala29" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="ioskala29" value="5" data-toggle="tooltip" title="u potpunosti se slažem"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>