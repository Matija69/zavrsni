<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $sseu = array();
            for($i = 1; $i <= 36; $i++) {
                $sseu[$i] = $_POST['sseu'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 127; $i <= 161; $i++) {
                $sql .= ("p" . $i . "='" . $sseu[$i - 126] . "',"); 
            }
            $sql .= ("p162='" . $sseu[36] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Doživljaji</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Dolje navedene tvrdnje opisuju neke načie na koje ljudi doživljavaju sebe
                        i svoj odnos s različitim drugim osobama. Pročitajte pažljivo svaku tvrdnju 
                        i procijenite koliko se ono što ona izriće odnosi na Vas osobno. Procjene ćete
                        davati tako što ćete uz svaku tvrdnju odabrati jedan od ponuđenih pet odgovora.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th class="textcentered"><h4 class="boldtext">Tvrdnja</h4></th>
                            <th>uopće se <br>ne slažem</th>
                            <th>donekle se <br>ne slažem</th>
                            <th>niti se slažem, <br>niti se ne slažem</th>
                            <th>uglavnom se <br>slažem</th>
                            <th>potpuno se <br>slažem</th>
                        </tr>
                        <tr>
                            <td>
                                1. Ono što je meni važno ne čini se da je važno ljudima koje znam.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu1" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu1" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu1" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu1" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu1" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Nemam prijatelja koji dijeli moja stajalšta.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu2" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu2" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu2" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu2" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu2" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Postoji krug prijatelja čiji sam ja dio.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu3" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu3" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu3" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu3" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu3" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Moji prijatelji razumiju moje potrebe i razmišljanja.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu4" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu4" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu4" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu4" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu4" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Uglavnom funkcioniram na istoj valnoj duljini s ljudima koje poznajem.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu5" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu5" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu5" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu5" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu5" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Imam puno zajedničkog s ljudima koje poznajem.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu6" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu6" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu6" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu6" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu6" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Imam prijatelje kojima se mogu obratiti za savjet.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu7" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu7" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu7" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu7" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu7" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Sviđaju mi se ljudi s kojima se družim.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu8" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu8" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu8" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu8" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu8" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Mogu računati na pomoć svojih prijatelja.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu9" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu9" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu9" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu9" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu9" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Imam prijatelje s kojima mogu razgovarati o onome što me tišti u mom životu.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu10" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu10" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu10" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu10" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu10" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                11. Nemam prijatelje koji me razumiju.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu11" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu11" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu11" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu11" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu11" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Nisam zadovoljan prijateljima koje imam.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu12" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu12" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu12" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu12" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu12" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Imam prijatelja s kojim mogu podijeliti svoje mišljenje.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu13" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu13" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu13" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu13" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu13" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Važan sam dio nečijeg života.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu14" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu14" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu14" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu14" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu14" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Osjećam se sam kada sam sa svojom obitelji.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu15" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu15" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu15" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu15" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu15" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Nikome u mojoj obitelji nije svarno stalo do mene.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu16" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu16" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu16" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu16" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu16" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                17. Imam ljubavnu vezu u kojoj s partnerom dijelim najskrovitije misli i osjećaje.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu17" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu17" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu17" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu17" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu17" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                18. Ne postoji nitko u mojoj obitelji na koga se mogu osloniti kad mi treba potpora i ohrabrenje.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu18" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu18" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu18" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu18" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu18" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                19. Zaista mi je stalo do moje obitelji.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu19" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu19" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu19" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu19" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu19" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                20. Postoji netko tko želi svoj život dijeliti sa mnom.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu20" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu20" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu20" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu20" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu20" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                21. Osjećam da doista pripadam u svoju obitelj.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu21" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu21" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu21" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu21" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu21" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                22. Imam potrebu za bliskom ljubavnom vezom koju do sada nisam zadovoljio.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu22" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu22" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu22" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu22" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu22" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                23. Želio bih da mogu reći nekome u koga sam zaljubljen da ga volim.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu23" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu23" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu23" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu23" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu23" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                24. Imam ljubavnu vezu s osobom koja mi pruža podršku ohrabrenje koje trebam.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu24" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu24" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu24" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu24" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu24" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                25. Zateknem sebe ponekad kako čeznem za nekim s kim bih dijelio svoj život.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu25" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu25" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu25" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu25" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu25" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                26. Želio bih da je mojoj obitelji više stalo do moje sreće.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu26" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu26" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu26" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu26" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu26" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                27. Zaljubljen sam u nekoga tko je također zaljubljen u mene.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu27" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu27" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu27" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu27" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu27" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                28. Osjećam se blizak svojoj obitelji.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu28" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu28" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu28" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu28" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu28" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                29. Imam nekoga tko zadovoljava moje potrebe za bliskošću.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu29" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu29" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu29" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu29" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu29" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                30. Osjećam se dijelom svoje obitelji.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu30" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu30" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu30" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu30" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu30" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                31. Postoji netko tko zadovoljava moje emocionalne potrebe.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu31" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu31" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu31" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu31" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu31" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                32. Ni s kim u obitelji nisam blizak.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu32" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu32" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu32" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu32" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu32" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                33. U ljubavnoj sam vezi s osobom čijoj sreći ja doprinosim.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu33" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu33" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu33" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu33" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu33" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                34. Moja mi je obitelj važna.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu34" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu34" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu34" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu34" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu34" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                35. Želio bih imati ljubavnu vezu koja bi mi pružila više zadovoljstva.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu35" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu35" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu35" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu35" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu35" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                        <tr>
                            <td>
                                36. Mojoj je obitelji doista stalo do mene.
                            </td>
                            <td class="textcentered"><input type="radio" name="sseu36" value="1" data-toggle="tooltip" title="uopće se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu36" value="2" data-toggle="tooltip" title="donekle se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu36" value="3" data-toggle="tooltip" title="niti se slažem, niti se ne slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu36" value="4" data-toggle="tooltip" title="uglavnom se slažem"></td>
                            <td class="textcentered"><input type="radio" name="sseu36" value="5" data-toggle="tooltip" title="potpuno se slažem"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>