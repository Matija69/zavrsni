<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $stupanj_vaznosti = array();
            for($i = 1; $i <= 6; $i++) {
                $stupanj_vaznosti[$i] = $_POST['stupanj_vaznosti'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 28; $i <= 32; $i++) {
                $sql .= ("p" . $i . "='" . $stupanj_vaznosti[$i - 27] . "',"); 
            }
            $sql .= ("p33='" . $stupanj_vaznosti[6] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
	include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Stupanj važnosti</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Molimo Vas da pažljivo pročitate opis pojedinih situacija te da u odgovarajući stupac
                        ispod <span class="boldtext">Stupnja važnosti</span> označite koliko je prema Vašem mišljenju 
                        važno raditi sljedeće stvari.
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Prema vašem mišljenju koliko je važno</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Stupanj važnosti</h4></th>
                        </tr>
                        <tr>
                            <th>potpuno nevažno</th>
                            <th>prilično nevažno</th>
                            <th>ne znam</th>
                            <th>prilično važno</th>
                            <th>izrazito važno</th>
                        </tr>
                        <tr>
                            <td>
                                11. Kopirati važnije dokumente na još jednu, drugu lokaciju odnosno drugi memorijski 
                                uređaj (izrada pričuvnih kopija podataka)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti1" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                12. Provjeriti tuđi USB memorijski štapić (stick) od virusa prije učitavanja podataka
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti2" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                13. Bezuvjetno čuvati tajnost svojih zaporki (lozinki)
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti3" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                14. Periodično zamijeniti svoje zaporke novima, barem za važnije sustave
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti4" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                15. Odvajati podatke prikupljene za školu od
                                osobnih/privatnih podataka (npr. prijenosna
                                memorija, elektronička pošta i slično).
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti5" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                        <tr>
                            <td>
                                16. Čuvati od krađe svoj USB memorijski štapić s važnim podacima
                            </td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="1" data-toggle="tooltip" title="potpuno nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="2" data-toggle="tooltip" title="prilično nevažno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="3" data-toggle="tooltip" title="ne znam"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="4" data-toggle="tooltip" title="prilično važno"></td>
                            <td class="textcentered"><input type="radio" name="stupanj_vaznosti6" value="5" data-toggle="tooltip" title="izrazito važno"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Sljedeći korak >>" name="dalje" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) { 
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault();
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>