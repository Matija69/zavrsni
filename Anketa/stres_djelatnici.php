<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['dalje'])) {
            session_start();
            $studentID = $_SESSION['sid'];
            include $_SESSION['konekcija'];
            $stres = array();
            for($i = 1; $i <= 10; $i++) {
                $stres[$i] = $_POST['stres'.$i];
            }
            $sql = "UPDATE {$_SESSION['table_name']} SET ";
            for($i = 65; $i <= 73; $i++) {
                $sql .= ("p" . $i . "='" . $stres[$i - 64] . "',"); 
            }
            $sql .= ("p74='" . $stres[10] . "' WHERE sID='" . $studentID . "'");
            mysqli_query($con, $sql);
            header('Location: ' . next($_SESSION['order']));
        } 
    }
    include 'referer.php';
?>
<!DOCTYPE html>
<html lang="hr">
    <head>
        <title>Skala stresa</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/bootstrap.min.js"></script>
        <script>
            window.history.forward();
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3></h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <div class="contentbox">
                    <h4>
                        Ovim upitnikom želimo provjeriti kako ste se osjećali i kakva su vam bila razmišljanja u posljednjih mjesec dana. 
                        Molimo vas da na svako pitanje odgovorite odabirom odgovora iz tablice koji smatrate točnim za vas. 
                    </h4>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="textcentered"><h4 class="boldtext">Skala stresa</h4></th>
                            <th colspan="5" class="textcentered"><h4 class="boldtext">Odaberi odgovarajući odgovor</h4></th>
                        </tr>
                        <tr>
                            <th>nikad</th>
                            <th>vrlo rijetko</th>
                            <th>ponekad</th>
                            <th>često</th>
                            <th>vrlo često</th>
                        </tr>
                        <tr>
                            <td>
                                1. Koliko ste često u posljednjih mjesec dana bili uznemireni zbog nečeg što se dogodilo nepredviđeno?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres1" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres1" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres1" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres1" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres1" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                2. Koliko ste se često u posljednjih mjesec dana osjećali nesposobnima da kontrolirate bitne stvari u svom životu?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres2" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres2" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres2" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres2" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres2" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                3. Koliko ste se često u posljednjih mjesec dana osjećali nervozno i pod “stresom” ?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres3" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres3" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres3" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres3" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres3" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                4. Koliko ste često u posljednjih mjesec dana bili sigurni u svoje sposobnosti za nošenje sa svojim osobnim problemima?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres4" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres4" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres4" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres4" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres4" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                5. Koliko ste često u posljednjih mjesec dana imali osjećaj da se stvari odvijaju po planu?                                                       
                            </td>
                            <td class="textcentered"><input type="radio" name="stres5" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres5" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres5" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres5" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres5" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                6. Koliko ste često u posljednjih mjesec dana ustanovili da ne možete obaviti sve one stvari koje biste morali?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres6" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres6" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres6" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres6" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres6" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                7. Koliko ste često u posljednjih mjesec dana mogli kontrolirati neugodnosti u Vašem životu?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres7" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres7" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres7" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres7" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres7" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                8. Koliko ste se često u posljednjih mjesec dana osjećali da imate potpunu kontrolu nad događanjima?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres8" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres8" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres8" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres8" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres8" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                9. Koliko ste  često u posljednjih mjesec dana bili ljuti zbog stvari koje su se dogodile van vaše kontrole?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres9" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres9" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres9" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres9" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres9" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                        <tr>
                            <td>
                                10. Koliko ste se često u posljednjih mjesec dana osjećali da ne možete prevladati nagomilane poteškoće?
                            </td>
                            <td class="textcentered"><input type="radio" name="stres10" value="1" data-toggle="tooltip" title="nikad"></td>
                            <td class="textcentered"><input type="radio" name="stres10" value="2" data-toggle="tooltip" title="vrlo rijetko"></td>
                            <td class="textcentered"><input type="radio" name="stres10" value="3" data-toggle="tooltip" title="ponekad"></td>
                            <td class="textcentered"><input type="radio" name="stres10" value="4" data-toggle="tooltip" title="često"></td>
                            <td class="textcentered"><input type="radio" name="stres10" value="5" data-toggle="tooltip" title="vrlo često"></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input type="submit" value="Završi anketu" name="kraj" class="btn btn-primary">
            </form>
        </div>
        <script>
            $(document).ready(function(){
				$("td").click(function () {
				   $(this).find('input:radio').attr('checked', true);
				});
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                $('form').submit(function(e) {
                    $(':radio').each(function() {
                        var groupname = $(this).attr('name');
                        if(!$(':radio[name="' + groupname + '"]:checked').length) {
                            e.preventDefault(); 
                            $(this).focus();
                            alert("Na jedno ili više pitanja nije odgovoreno. Odgovorite na sva pitanja, molim.");
                            return false;
                        }
                    });
                });
            });
        </script>
    </body>
</html>